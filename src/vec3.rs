use num_traits::Float;

use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

pub struct Vec3<T>(pub(crate) T, pub(crate) T, pub(crate) T);

impl<T> Vec3<T> {
    #[inline]
    pub fn new(x: T, y: T, z: T) -> Self {
        Vec3(x, y, z)
    }
}

impl<T> Vec3<T>
where
    T: Add<T, Output = T> + Mul<T, Output = T> + Copy,
{
    #[inline]
    pub fn norm2(&self) -> T {
        self.0 * self.0 + self.1 * self.1 + self.2 * self.2
    }
}

impl<T> Vec3<T>
where
    T: Add<T, Output = T> + Mul<T, Output = T> + Float,
{
    #[inline]
    pub fn norm(&self) -> T {
        self.norm2().sqrt()
    }

    pub fn normalise(&self) -> Self {
        let norm = self.norm();
        Vec3(self.0 / norm, self.1 / norm, self.2 / norm)
    }
}

impl<T, R, O> Mul<Vec3<R>> for Vec3<T>
where
    T: Mul<R, Output = O>,
{
    type Output = Vec3<O>;

    fn mul(self, rhs: Vec3<R>) -> Self::Output {
        Vec3(self.0 * rhs.0, self.1 * rhs.1, self.2 * rhs.2)
    }
}

impl<T, R> MulAssign<Vec3<R>> for Vec3<T>
where
    T: MulAssign<R>,
{
    fn mul_assign(&mut self, rhs: Vec3<R>) {
        self.0 *= rhs.0;
        self.1 *= rhs.1;
        self.2 *= rhs.2;
    }
}

impl<T, R, O> Add<Vec3<R>> for Vec3<T>
where
    T: Add<R, Output = O>,
{
    type Output = Vec3<O>;

    fn add(self, rhs: Vec3<R>) -> Self::Output {
        Vec3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
    }
}

impl<T, R> AddAssign<Vec3<R>> for Vec3<T>
where
    T: AddAssign<R>,
{
    fn add_assign(&mut self, rhs: Vec3<R>) {
        self.0 += rhs.0;
        self.1 += rhs.1;
        self.2 += rhs.2;
    }
}

impl<T, R, O> Sub<Vec3<R>> for Vec3<T>
where
    T: Sub<R, Output = O>,
{
    type Output = Vec3<O>;

    fn sub(self, rhs: Vec3<R>) -> Self::Output {
        Vec3(self.0 - rhs.0, self.1 - rhs.1, self.2 - rhs.2)
    }
}

impl<T, R> SubAssign<Vec3<R>> for Vec3<T>
where
    T: SubAssign<R>,
{
    fn sub_assign(&mut self, rhs: Vec3<R>) {
        self.0 -= rhs.0;
        self.1 -= rhs.1;
        self.2 -= rhs.2;
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
