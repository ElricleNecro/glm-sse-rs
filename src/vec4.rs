#[cfg(all(target_arch = "x86", target_feature = "sse2"))]
use std::arch::x86::{_mm_div_ps, _mm_mul_ps, _mm_set_ps};
#[cfg(all(target_arch = "x86_64", target_feature = "sse2"))]
use std::arch::x86_64::{_mm_div_ps, _mm_mul_ps, _mm_set1_ps, _mm_set_ps};

use std::convert::From;
use std::mem::transmute;
use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};

use super::vec3::Vec3;

pub struct Vec4<T>(pub(crate) T, pub(crate) T, pub(crate) T, pub(crate) T);

impl<T> Vec4<T> {
    #[inline]
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Vec4(x, y, z, w)
    }
}

impl Vec4<f32> {
    #[inline]
    #[cfg(not(all(
        any(target_arch = "x86", target_arch = "x86_64"),
        target_feature = "sse2"
    )))]
    pub fn norm2(&self) -> f32 {
        self.0 * self.0 + self.1 * self.1 + self.2 * self.2 + self.3 * self.3
    }

    #[inline]
    #[cfg(all(
        any(target_arch = "x86", target_arch = "x86_64"),
        target_feature = "sse2"
    ))]
    pub fn norm2(&self) -> f32 {
        let res: [f32; 4] = unsafe {
            transmute(_mm_mul_ps(
                _mm_set_ps(self.0, self.1, self.2, self.3),
                _mm_set_ps(self.0, self.1, self.2, self.3),
            ))
        };

        res.iter().sum()
    }

    #[inline]
    pub fn norm(&self) -> f32 {
        self.norm2().sqrt()
    }

    #[cfg(not(all(
        any(target_arch = "x86", target_arch = "x86_64"),
        target_feature = "sse2"
    )))]
    pub fn normalise(&self) -> Self {
        let norm = self.norm();
        Vec4(self.0 / norm, self.1 / norm, self.2 / norm, self.3 / norm)
    }

    #[cfg(all(
        any(target_arch = "x86", target_arch = "x86_64"),
        target_feature = "sse2"
    ))]
    pub fn normalise(&self) -> Self {
        let norm = self.norm();
        unsafe {
            transmute(_mm_div_ps(
                _mm_set_ps(self.0, self.1, self.2, self.3),
                _mm_set1_ps(norm),
            ))
        }
    }
}

impl<T> From<Vec3<T>> for Vec4<T>
where
    T: Copy + From<i8>,
{
    fn from(val: Vec3<T>) -> Self {
        Vec4(val.0, val.1, val.2, 1.into())
    }
}

impl<T, R, O> Mul<Vec4<R>> for Vec4<T>
where
    T: Mul<R, Output = O>,
{
    type Output = Vec4<O>;

    fn mul(self, rhs: Vec4<R>) -> Self::Output {
        Vec4(
            self.0 * rhs.0,
            self.1 * rhs.1,
            self.2 * rhs.2,
            self.3 * rhs.3,
        )
    }
}

impl<T, R> MulAssign<Vec4<R>> for Vec4<T>
where
    T: MulAssign<R>,
{
    fn mul_assign(&mut self, rhs: Vec4<R>) {
        self.0 *= rhs.0;
        self.1 *= rhs.1;
        self.2 *= rhs.2;
        self.3 *= rhs.3;
    }
}

impl<T, R, O> Add<Vec4<R>> for Vec4<T>
where
    T: Add<R, Output = O>,
{
    type Output = Vec4<O>;

    fn add(self, rhs: Vec4<R>) -> Self::Output {
        Vec4(
            self.0 + rhs.0,
            self.1 + rhs.1,
            self.2 + rhs.2,
            self.3 + rhs.3,
        )
    }
}

impl<T, R> AddAssign<Vec4<R>> for Vec4<T>
where
    T: AddAssign<R>,
{
    fn add_assign(&mut self, rhs: Vec4<R>) {
        self.0 += rhs.0;
        self.1 += rhs.1;
        self.2 += rhs.2;
        self.3 += rhs.3;
    }
}

impl<T, R, O> Sub<Vec4<R>> for Vec4<T>
where
    T: Sub<R, Output = O>,
{
    type Output = Vec4<O>;

    fn sub(self, rhs: Vec4<R>) -> Self::Output {
        Vec4(
            self.0 - rhs.0,
            self.1 - rhs.1,
            self.2 - rhs.2,
            self.3 - rhs.3,
        )
    }
}

impl<T, R> SubAssign<Vec4<R>> for Vec4<T>
where
    T: SubAssign<R>,
{
    fn sub_assign(&mut self, rhs: Vec4<R>) {
        self.0 -= rhs.0;
        self.1 -= rhs.1;
        self.2 -= rhs.2;
        self.3 -= rhs.3;
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
